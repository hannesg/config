require('flit').setup()

require("noice").setup({
  lsp = {
    -- override markdown rendering so that **cmp** and other plugins use **Treesitter**
    override = {
      ["vim.lsp.util.convert_input_to_markdown_lines"] = true,
      ["vim.lsp.util.stylize_markdown"] = true,
      ["cmp.entry.get_documentation"] = true,
    },
  },
  -- you can enable a preset for easier configuration
  presets = {
    bottom_search = true, -- use a classic bottom cmdline for search
    command_palette = true, -- position the cmdline and popupmenu together
    long_message_to_split = true, -- long messages will be sent to a split
    inc_rename = false, -- enables an input dialog for inc-rename.nvim
    lsp_doc_border = false, -- add a border to hover docs and signature help
  },
})
-- Which-key
local wk = require("which-key")
wk.setup() 

-- Find stuff
vim.keymap.set("n","<leader>s",function() require("telescope.builtin").find_files() end, {desc="Find files"})
vim.keymap.set("n","<leader>g",function() require("telescope.builtin").live_grep() end, {desc="Grep"})
vim.keymap.set("n","<leader>b",function() require("telescope.builtin").buffers() end, {desc="Buffers"})

-- Tree
vim.keymap.set("n","<leader>o",function() require("nvim-tree.api").tree.toggle({find_file=true}) end, {desc="Open + focus the tree"})

-- Move to window using the <ctrl> hjkl keys
local smart_splits = require('smart-splits')
vim.keymap.set("n", "<C-h>", smart_splits.move_cursor_left, { desc = "Go to left window" })
vim.keymap.set("n", "<C-j>", smart_splits.move_cursor_down, { desc = "Go to lower window" })
vim.keymap.set("n", "<C-k>", smart_splits.move_cursor_up, { desc = "Go to upper window" })
vim.keymap.set("n", "<C-l>", smart_splits.move_cursor_right, { desc = "Go to right window" })

-- better indenting
vim.keymap.set("v", "<", "<gv")
vim.keymap.set("v", ">", ">gv")

vim.keymap.set("n", "<leader>n", "<cmd>enew<cr>", { desc = "New File" })


-- Goto
wk.add({
  {"g", {group = "Goto"}},
  {"gc",{desc = "Comment toggle linewise"}},
  {"gb",{desc = "Comment toggle blockwise"}},
})
vim.keymap.set("n", "gd", "<cmd>Telescope lsp_definitions<cr>", {desc = "Goto Definition" })
vim.keymap.set("n", "gr", "<cmd>Telescope lsp_references<cr>", {desc = "References" })

-- Code
vim.keymap.set({"n","v"}, "<leader>a", "<cmd>Lspsaga code_action<cr>", {desc = "Code Action"})
vim.keymap.set({"n","v"}, "<leader>i", "<cmd>Lspsaga outline<cr>", {desc = "Outline"})
vim.keymap.set({"n","v"}, "<leader>r", vim.lsp.buf.rename, {desc = "Code Rename"})
vim.keymap.set({"n","v"}, "<leader>f", function() vim.lsp.buf.format { async = true } end, {desc = "Code Format" })

-- Commenting
vim.keymap.set({"n","v"}, "<leader>/", function() require("Comment.api").locked("toggle.linewise")(vim.fn.visualmode()) end, {desc = "Comment" })

vim.keymap.set({"n","v"}, "<S-h>", "<cmd>bprev<cr>", {desc = "Previous Buffer" })
vim.keymap.set({"n","v"}, "<S-l>", "<cmd>bnext<cr>", {desc = "Next Buffer" })


vim.keymap.set({"n","v"}, "<C-s>", "<cmd>w<cr>", {desc = "Save"})

-- Auto-paranthese
-- If you want insert `(` after select function or method item
local cmp_autopairs = require('nvim-autopairs.completion.cmp')
local cmp = require('cmp')
cmp.event:on(
  'confirm_done',
  cmp_autopairs.on_confirm_done()
)

vim.cmd [[
  highlight! DiagnosticLineNrError guifg=#FF0000 gui=bold
  highlight! DiagnosticLineNrWarn guifg=#FFA500 gui=bold
  highlight! DiagnosticLineNrInfo guifg=#00FFFF gui=bold
  highlight! DiagnosticLineNrHint guifg=#0000FF gui=bold

  sign define DiagnosticSignError text= texthl=DiagnosticSignError linehl= numhl=DiagnosticLineNrError
  sign define DiagnosticSignWarn text= texthl=DiagnosticSignWarn linehl= numhl=DiagnosticLineNrWarn
  sign define DiagnosticSignInfo text= texthl=DiagnosticSignInfo linehl= numhl=DiagnosticLineNrInfo
  sign define DiagnosticSignHint text= texthl=DiagnosticSignHint linehl= numhl=DiagnosticLineNrHint
]]
