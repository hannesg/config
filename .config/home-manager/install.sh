wb=$(find "$wrapper"/bin/ -type f -executable)

echo "Using wrapper $wb"

wrapperContent=$(sed -e 1d -e \$d "$wb")

declare -a srcsArray;
declare -a outputsArray;
if [ -n "$__structuredAttrs" ]; then
    srcsArray=("${srcs[@]}");
else
    read -ra srcsArray <<<"$srcs"
    read -ra outputsArray <<<"$outputs";
fi;

function _patchDir() {
  local in="$1"
  local out="$2"
  echo "$in -> $out"
  if [ ! -d "$in" ]; then
    cp "$in" "$out"
    return 
  fi
  cp -r "$in" "$out"
  local bin="$out"/bin
  if [ -d "$bin" ] ; then
    chmod -R u+w "$bin"
    for i in "$bin"/*;
    do
      wrapProgram "$i" --run "$wrapperContent"
    done
  fi
}

for i in "${!srcsArray[@]}";
do
  _patchDir "${srcsArray[$i]}" "${!outputsArray[$i]}"
done; 
