{
  description = "Home configs";

  inputs.nixpkgs.url = "github:NixOS/nixpkgs";

  inputs.homeManager.url = "github:nix-community/home-manager";
  inputs.homeManager.inputs.nixpkgs.follows = "nixpkgs";

  inputs.nixvim = {
    url = "github:nix-community/nixvim";
    inputs.nixpkgs.follows = "nixpkgs";
  };

  outputs = { self, nixpkgs, homeManager, nixvim }:
    let
      system = "x86_64-linux";
      pkgs = import nixpkgs {
        inherit system;
        config.allowUnfreePredicate = pkg:
          builtins.elem (nixpkgs.lib.getName pkg) [
            "vscode" # vscode is needed for cssls lsp server
            "terraform"
          ];
      };
      sharedPkg = pkgs: [
        pkgs.tmux
        pkgs.stdenv.cc.cc.lib
        pkgs.fzf
        pkgs.nixVersions.stable
        pkgs.fish
        pkgs.fzf
        pkgs.ripgrep
        pkgs.fd
        pkgs.yq-go
        pkgs.shellcheck
        pkgs.tree

        pkgs.jq

        # nodejs
        pkgs.nodePackages.pnpm

        # misc
        pkgs.nixfmt-classic
        pkgs.nix-output-monitor
      ];
      wezterm = { home.packages = [ pkgs.wezterm ]; };

      rust = {
        programs.nixvim = {
          plugins.lsp = {
            servers.rust_analyzer = {
              enable = true;
	      installCargo = true;
	      installRustc = true;
            };
          };
        };
	home.packages = with pkgs; [rustc cargo mold llvmPackages.libcxxClang pkg-config alsa-lib.dev];
      };
      nvim = {
        programs.nixvim = {
          enable = true;

          colorschemes.tokyonight = {
            enable = true;
            settings.style = "night";
            settings.transparent = true;
          };

          globals = { mapleader = " "; };

          plugins.telescope = { enable = true; };

          plugins.treesitter = {
            enable = true;
            settings.indent = { enable = true; };
          };

          plugins.lsp = {
            enable = true;
            #onAttach = ''
            #  	      require("lsp-format").on_attach(client)
            # 	    '';
            servers = {
              gopls.enable = true;
              bashls.enable = true;
              nixd.enable = true;
              cssls.enable = true;
              ts_ls.enable = true;
              terraformls = {
                enable = true;
                cmd = [ "${pkgs.terraform-ls}/bin/terraform-ls" "serve" ];
              };
              yamlls.enable = true;
              jsonls.enable = true;
            };
          };

          plugins.comment = { enable = true; };

          plugins.nvim-tree = {
            enable = true;
            git.enable = true;
            syncRootWithCwd = true;
          };

          plugins.web-devicons.enable = true;

          plugins.nvim-autopairs = { enable = true; };

          plugins.indent-blankline = {
            enable = true;
            settings.scope.enabled = true;
          };

          plugins.lspkind = {
            enable = true;
            mode = "symbol";
            cmp.after = ''
              function(entry, vim_after, kind)
                kind.dup = 0
                return kind
              end
            '';
          };

          plugins.luasnip = { enable = true; };

          plugins.cmp = {
            enable = true;
            cmdline = {
              "/" = { sources = [{ name = "buffer"; }]; };
              ":" = { sources = [ { name = "path"; } { name = "cmdline"; } ]; };
            };
            settings = {
              snippet.expand =
                "function(args) require('luasnip').lsp_expand(args.body) end";
              sources = [
                { name = "nvim_lsp"; }
                { name = "nvim_lsp_document_symbol"; }
                { name = "treesitter"; }
                { name = "nvim_lsp_signature_help"; }
                { name = "luasnip"; }
                { name = "path"; }
                { name = "buffer"; }
              ];
              window = {
                completion = {
                  winhighlight = "Normal:Pmenu,FloatBorder:Pmenu,Search:None";
                  colOffset = -1;
                  sidePadding = 0;
                };
                documentation = { };
              };
              formatting = { fields = [ "kind" "abbr" "menu" ]; };
              mapping = {
                "<CR>" = "cmp.mapping.confirm({ select = true })";
                "<Tab>" = ''
                  function(fallback)
                    if cmp.visible() then
                      cmp.select_next_item()
                    else
                      fallback()
                    end
                  end
                '';
                "<S-Tab>" = ''
                  function(fallback)
                    if cmp.visible() then
                      cmp.select_prev_item()
                    else
                      fallback()
                    end
                  end
                '';
              };
            };
          };

          plugins.leap = {
            enable = true;
            highlightUnlabeledPhaseOneTargets = true;
          };

          plugins.lspsaga = {
            enable = true;
            lightbulb = { enable = false; };
          };

          extraPlugins = with pkgs.vimPlugins; [
            which-key-nvim
            nui-nvim
            noice-nvim
            smart-splits-nvim
            committia
            lsp-format-nvim
            flit-nvim
          ];

          opts = {
            number = true;
            relativenumber = true; # Show relative line numbers
            shiftwidth = 2; # Tab width should be 2
            timeout = true;
            timeoutlen = 500;
            list = true;
            listchars = "eol:↴,trail:-,space: ,tab:⋅ ";
          };

          keymaps = [
            {
              key = "<SPACE>";
              action = "<NOP>";
            }
            {
              key = "gl";
              action.__raw = "vim.diagnostic.open_float";
            }
            {
              key = "K";
              action.__raw = "vim.lsp.buf.hover";
            }
          ];

          extraConfigLuaPost = builtins.readFile ./extra.lua;
        };
        imports = [ nixvim.homeManagerModules.nixvim ];
      };
      direnv = {
        programs.direnv.enable = true;
        programs.direnv.nix-direnv.enable = true;
      };
      home = username: {
        home.username = username;
        home.homeDirectory = "/home/${username}";
      };
      fonts = {
        fonts.fontconfig.enable = true;
        home.packages = [
          pkgs.nerd-fonts.ubuntu-mono
          pkgs.nerd-fonts._0xproto
          pkgs.nerd-fonts.agave
        ];
      };
      term = {
        dconf.settings = {
          "org/gnome/terminal/legacy/profiles:/:00000000-0000-0000-0000-000000000000" =
            {
              audible-bell = false;
              background-color = "rgb(17,17,17)";
              cursor-shape = "ibeam";
              custom-command = "${pkgs.tmux}/bin/tmux -2 -u";
              font = "Hack Nerd Font Mono 12";
              foreground-color = "rgb(208,207,204)";
              login-shell = false;
              palette = [
                "rgb(21,22,30)"
                "rgb(247,118,142)"
                "rgb(158,206,106)"
                "rgb(224,175,104)"
                "rgb(122,162,247)"
                "rgb(187,154,247)"
                "rgb(125,207,255)"
                "rgb(169,177,214)"
                "rgb(65,72,104)"
                "rgb(251,70,97)"
                "rgb(158,235,76)"
                "rgb(242,214,171)"
                "rgb(59,120,252)"
                "rgb(137,77,249)"
                "rgb(51,199,222)"
                "rgb(255,255,255)"
              ];
              preserve-working-directory = "always";
              scroll-on-keystroke = false;
              scrollback-unlimited = true;
              scrollbar-policy = "never";
              use-custom-command = true;
              use-system-font = false;
              use-theme-colors = false;
              visible-name = "hag";
            };
          "org/gnome/terminal/legacy/profiles:" = {
            list = [ "00000000-0000-0000-0000-000000000000" ];
            default = "00000000-0000-0000-0000-000000000000";
          };
        };
      };
    in {
      homeConfigurations = {
        "hag@hag" = homeManager.lib.homeManagerConfiguration {
          inherit pkgs;

          modules = [
            nvim
            direnv
            fonts
            wezterm
	    rust
            (home "hag")
            {
              home.packages = (sharedPkg pkgs);
              programs.home-manager = { enable = true; };
              home.stateVersion = "25.05";
            }
          ];
        };
        "hannesgeorg@hannesgeorg-ThinkPad-P1-Gen-3" =
          homeManager.lib.homeManagerConfiguration {
            inherit pkgs;
            modules = [
              nvim
              direnv
              (home "hannesgeorg")
              {
                home.packages = (sharedPkg pkgs) ++ [ pkgs.terraform ];
                programs.home-manager = { enable = true; };
                home.stateVersion = "22.05";
              }
            ];
          };
      };
    };
}
