{stdenv, makeWrapper, ...}: wrapper: pkg: let

in stdenv.mkDerivation {
  name = "${pkg.name}+${wrapper.name}";
  outputs = pkg.outputs;
  nativeBuildInputs = [makeWrapper];
  
  wrapper = wrapper;
  
  srcs = map (a: builtins.getAttr a pkg) pkg.outputs;
  sourceRoot = "."; 
  installPhase = builtins.readFile ./install.sh;
  dontBuild = true;
  dontStrip = true;
  dontFixup = true;
}
